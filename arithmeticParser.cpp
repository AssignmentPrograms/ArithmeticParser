

#include <iostream>
#include <cstdlib>
using std::cout;

const char* ch;

int parseFormula();
int parseSum();
int parseProduct();
int parseFactor();


int parseFormula()
{
    int result = 0;
    //while( *ch != '\n' )
    {
        if( *ch >= '0' && *ch <= '9' )
        {
            result += parseSum();
        }
        else
        {
            cout << "Error? parseFormula\n";
            exit(1);
        }
    }

    #ifdef DEBUG
        cout << "parseFormula: " << result << "\n";
    #endif

    return result;
}

int parseSum()
{
    int prodOne = parseProduct();

    while( *ch == '+' )
    {
        ch++;
        int prodTwo = parseProduct();
        prodOne += prodTwo;
    }

    #ifdef DEBUG
        cout << "THINGY: " << *ch << std::endl;
        cout << "parseSum: " << prodOne << "\n";
    #endif

    return prodOne;
}

int parseProduct()
{
    int factorOne = parseFactor();

    while( *ch == '*' )
    {
        ch++;
        int factorTwo = parseFactor();
        factorOne *= factorTwo;
    }

    #ifdef DEBUG
        cout << "parseProduct: " << factorOne << "\n";
    #endif

    return factorOne;
}

int parseFactor()
{
    int factor = 0;
    if( *ch >= '0' &&  *ch <= '9' )
    {
        while( *ch >= '0' && *ch <= '9' )
        {
            factor = (factor * 10) + *ch - '0';
            ch++;
        }
    }
    else if( *ch == '(' )
    {
        ch++; // consume '('
        factor = parseSum();
        ch++; // consume ')'
    }

    return factor;
}



int main()
{
    ch = "4+1+(2+3+(2+3*(2+1)))";
    cout << "Answer = " << parseFormula() << "\n";
    return 0;
}
